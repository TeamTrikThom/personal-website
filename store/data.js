export const state = () => ({
  projects: [
    {name: 'Lego Set Builder', category: 'WEB DESIGN', img: '/img/lego.jpg', link: '/software/lego', text: 'A lego sorting website that calculates which block belongs to which sets.'},
    {name: 'Friction', category: 'WEB DESIGN', img: '/img/friction.png', link: '/projects/friction', text: 'Integrated test with Evert Devos for school'},
    {name: 'Kit PvP', category: 'JAVA', img: '/favicon.ico', link: '/software/kitpvp', text: 'Just an ordenary Kit Pvp plugin for Minecraft, special made for my server.'},
    {name: 'NDI', category: 'WEB DESIGN', img: '/img/nauticduikersclubizegem.png', link: '/software/ndi', text: 'Website for a dive club.'}
  ]
})

export const getters = {
  projects: (state) => {
    return state.projects
  },
  topProjects: (state) => {
    return state.projects.slice(0, 3)
  }
}

export const mutations = {
}

export const actions = {
}
