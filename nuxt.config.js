
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'TrikThom',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'This is my personal website' },
      { name: 'description', name: 'description', content: 'This is my personal website' },
      { lang: 'en' },
      { name: 'google-site-verification', content: 'sDqIHfGOmL226RDlQXT3-ZBztQp0Rg7Dozalis3rH9E' }

    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'manifest', href: '/manifest.json' }
    ]
  },
  plugins: [
    { src: '~/plugins/vuetify.js', ssr: true }
  ],
  router: {
    middleware: []
  },
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#1867c0' },
  modules: [
    '@nuxtjs/proxy'
  ],
  proxy: {},
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      'vuetify'
    ],
    extractCSS: true,
    cssSourceMap: false,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
